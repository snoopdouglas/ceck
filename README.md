# ceck

Tiny Python daemon which translates common TV remote button-presses into keypresses, via HDMI-CEC.

* Media controls - play/pause/forwards/backwards/stop - will function as if they'd been pressed from the keyboard.
* Up/down/left/right will work as cursor keys.
* The 'OK' button presses Enter.
* The 'Exit' button presses Esc.

Kodi's implementation of this is (perhaps obviously) way better, but this is just a quick thing I made to get my TV interacting with my laptop.

## Setup

### Caveats

* Your computer *probably* doesn't have a CEC interface, unless you're on a Pi, which probably *does*. Otherwise, you may want to [buy one](https://www.pulse-eight.com/p/104/usb-hdmi-cec-adapter).
* ceck uses the `keyboard` package, which **requires root** for `/dev/input` access (sorry). So, you should probably [read the source](./ceck) before you use this.
* It's been tested with Python 3.9.2+, but should (in theory) work all the way back on 3.6.
* A systemd unit is included (we install this below) - if you're fighting the good fight, obviously you'll want to skip it, but you'll need to write your own init config.

### Instructions

* You'll need `cec-client` installed:
  ```sh
  # ubuntu/debian
  sudo apt install cec-utils

  # fedora/rhel
  sudo dnf install libcec
  ```
* Install Python packages (as root):
  ```sh
  sudo pip3 install -r requirements.txt
  ```
* Install ceck. From the root of the repo:
  ```sh
  sudo install -t /opt ceck
  sudo install -t /etc/systemd/system ceck.service
  ```
* Activate the service:
  ```sh
  sudo systemctl enable ceck
  ```
